<?php
    
    $arrComment = [
        ['nom' => 'Skovald', 'prenom' => 'Jean-Didier', 'commentaire' => "C'est cool les array"],
        ['nom' => 'Fordragon', 'prenom' => 'Bolvar', 'commentaire' => "C'est cool les objets !"],
        ['nom' => 'Wulfstan', 'prenom' => 'Delvin', 'commentaire' => "C'est cool JS "],
        ['nom' => 'Rivebois', 'prenom' => 'Ralof', 'commentaire' => "C'est cool les Php"]
    ];
   
?>


<?php include './includes/header.php'?>
    <div class="container">
        <?php include './includes/search-header.php'?>
        <?php include './includes/form-comment.php'?>
    </div>
    <?php include './includes/commentaire.php'?>
    
    <?php include './includes/footer.php'?>